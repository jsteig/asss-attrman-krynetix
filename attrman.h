#ifndef ATTRMAN_H_INCLUDED
#define ATTRMAN_H_INCLUDED

/* pyinclude: attrman/attrman.h */

#define I_ATTRMAN "attrman-5"

#include <stdbool.h>

/**
    Attribute Manager $Revision: 353 $ by JoWie 
    jowie@welcome-to-the-machine.com

    Allows modules to register attributes which can then be set by other modules. For example a buy system which effects settings in other modules.

*/

typedef struct
{
	union {
		long number;
		void *pointer;
	};
	union {
		long number2;
		void *pointer2;
	};
} ParamAttrman;

#define ATTRMAN_NO_PARAM attrman->emptyParam

typedef unsigned long AttrmanSetter;

/** @threading called from any (caller of SetValue/UnsetValue) */
typedef void (*SetValueFunc)(const Target *scope, const char *attributeName, signed char ship, AttrmanSetter setter, ParamAttrman param);


typedef enum
{
	/* pyconst: enum, "ATTRMAN_OP_*" */
	ATTRMAN_OP_ERROR = -1,
	ATTRMAN_OP_NOTHING = 0,
	ATTRMAN_OP_UNSET,
	ATTRMAN_OP_ABSOLUTE,
	ATTRMAN_OP_ADDITION

} ATTRIBUTESTRING_OPERATION;

/* pyconst: define int, "ATTRMAN_*" */
#define ATTRMAN_NO_SHIP (-1)

typedef struct Iattrman
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	/** Always call Lock before using any method 
	 * The order to use is:
	 * 	1. individual player locks
	 * 	2. arena status
	 * 	3. player status	 
	 * 	4. attrman
	 */
	void (*Lock)();
	/* pyint: void -> void */
	
	/** Always call Unlock when done */
	void (*UnLock)();
	/* pyint: void -> void */

	/** Before a module may set a value it needs a setter.
	  *
	  * It does not really matter how you use this.
	  * You can use 1 setter per module or 1 setter per item in a buy sytem for example.
	  *
	  * The order of registered setters determines how you have precedence in GetTotalValue; The latest registered setters have higher precedence that the earliest
	  */
	AttrmanSetter (*RegisterSetter)();
	/* pyint: void -> uint */

	/** Every registered setter has to be unregistered at some point.
	  * This will free all the memory used and the values will no longer have effect.
	  *
	  * The setter will then be available for reuse. (using RegisterSetter)
	  */
	void (*UnregisterSetter)(AttrmanSetter setter);
	/* pyint: uint -> void */
	/**
	  *	Removes all values associated by this setter. However the setter can still be used.
	  *	Do not forget that if you use the same setter across arena's it will be cleared in all those arena's
	  */
	void (*UnsetAll)(AttrmanSetter setter);
	/* pyint: uint -> void */

	/** Register a callback;
	  * This only fires when an attribute changes using ->SetValue, not when it MIGHT change for a player due to an arena / freq / ship change
	  */
	void (*RegisterCallback)(const char *attributeName, SetValueFunc setValueCallback, ParamAttrman param);

	/** Unregister the callback, do this at module unload */
	void (*UnregisterCallback)(const char *attributeName, SetValueFunc setValueCallback);


	/**
	  * Sets the value of an attribute (overflow safe)
	  * Scope can be T_PLAYER, T_FREQ, T_ARENA, T_ZONE
	  * (Note: if an attribute is set to a player, it only works in the current arena he is in, if he leaves the arena he loses the attributes)
	  *
	  * if add is true, it will add the value, and not overwrite it.
	  *
	  * Optionally the attribute can be limited to a ship (0 is warbird). Use ATTRMAN_NO_SHIP if it's for all the ships
	  */
	void (*SetValue)(const Target *scope, AttrmanSetter setter, const char *attributeName, bool addition, signed char ship, long value);
	/* pyint: target, uint, string, int, int, int -> void */

	/**
	  * Removes the value of an attribute
	  * Scope can be T_PLAYER, T_FREQ, T_ARENA
	  * Returns the old value
	  *
	  * NOTE: unsetting with ship = ATTRMAN_NO_SHIP will _NOT_ unset the attribute for other ships
	  */
	long (*UnsetValue)(const Target *scope, AttrmanSetter setter, const char *attributeName, signed char ship);
	/* pyint: target, uint, string, int -> void */

	/**
	  * Gets the value of an attribute
	  * Scope can be T_PLAYER, T_FREQ, T_ARENA, T_ZONE
	  *
	  * Gets the value of a specific setter. A setter of 0 may also be used, this will use the first value found.
	  * This should only be used for attributes that should not be added up. (a timestamp for example)
	  *
	  * if recursive is set it will start looking up the scope if a value is not found
	  *
	  * It is recommended to use the form "Module::Attribute" for the attributeName
	  *
	  * NOTE: This function will NOT get the value of ATTRMAN_ANY_SHIP _if_ you set a ship, use GetTotalValue for that (or do it yourself)
	  */
	long (*GetValue)(const Target *scope, AttrmanSetter setter, const char *attributeName, bool recursive, signed char ship);
	/* pyint: target, uint, string, int, int -> int */

	/**
	  * Gets the value of an attribute
	  * Scope can be T_PLAYER, T_FREQ, T_ARENA, T_ZONE
	  *
	  * If recursive is true: All the values of EVERY scope are added up (except the scopes under this one):
	  * T_ZONE
	  * T_ARENA (+T_ZONE)
	  * T_FREQ (+T_ZONE+ARENA)
	  * T_PLAYER (+TFREQ+T_ARENA+T_ZONE)
	  *
	  *
	  * If recursive is false: Only the values of the CURRENT scope are added up
	  *
	  * For example GetValue(tgtFreq, "something") will add up the values of T_FREQ + T_ARENA + T_ZONE
	  *
	  * It is recommended to use the form "Module::Attribute" for the attributeName
	  */
	long (*GetTotalValue)(const Target *scope, const char *attributeName, signed char ship, /*bool*/ int *hasAbsoluteValue);
	/* pyint: target, string, int, int out -> int */

	/**
	  * Get the total recursive value for the specified player using his current ship
	  *
	  */
	long (*GetPlayerValue)(Player *p, const char *attributeName, /*bool*/ int *hasAbsoluteValue);
	/* pyint: player, string, int out -> int */

	/**
	  *	Allows you to break down config strings
	  *
	  *	Examples:
	  *
	  *	bla::myattribute = =5				(attribute = "bla::myattribute"; value = 5; operation = ATTRMAN_OP_ABSOLUTE)
	  *	bla::myattribute = +5				(value = 5; operation = ATTRMAN_OP_ADDITON)
	  *	bla::myattribute = 5				(value = 5; operation = ATTRMAN_OP_ADDITON)
	  *	bla::myattribute = -5				(value = -5; operation = ATTRMAN_OP_ADDITON)
	  *	bla::myattribute =				(operation = ATTRMAN_OP_UNSET)
	  *	bla::myattribute				(operation = ATTRMAN_OP_NOTHING)
	  *	terrier#bla::myattribute = +5			(value = 5; operation = ATTRMAN_OP_ADDITON; only for ship 5)
	  *	5#bla::myattribute = +5				(value = 5; operation = ATTRMAN_OP_ADDITON; only for ship 5)
	  *
	  *	Note: Hex and Octal are also supported for the value:
	  *
	  *	bla::myattrbite = 0xFE5				(value = 4069)
	  *	bla::myattrbite = 0371				(value = 249)
	  *
	  *
	  *	  	  
	  *	char attribute[256];
	  *	long value = 0;	  
	  *	int ship;
	  *	ATTRIBUTESTRING_OPERATION operation;	  
	  *	if (parseAttributeString("bla::myattribute = +5", attribute, 256, &value, &ship, &operation))
	  *	{
	  *		// ...	  
	  *	}	  	  
	  * (does not require a lock)	  
	  */
	bool (*parseAttributeString)(const char *str, char *attribute, int maxAttributeSize, long *value, int *ship, ATTRIBUTESTRING_OPERATION *operation);
	/* pyint: string, string out, int buflen, int out, int out, int out -> int */

	ParamAttrman emptyParam;
} Iattrman;

#endif // ATTRMAN_H_INCLUDED
