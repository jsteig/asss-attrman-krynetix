/*
ASSS Attribute Manager Position Rewrite
jowie@welcome-to-the-machine.com
Copyright (c) 2009-2011  Joris v/d Wel

This file is part of ASSS Attribute Manager

   ASSS Attribute Manager is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Attribute Manager is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with ASSS Attribute Manager.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms apply, based on section 7 of the
   GNU Affero General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version


*/
#include <stdio.h>
#include <limits.h>

#include "asss.h"
#include "attrman.h"

static Imodman *mm;
static Igame* game;
static Iconfig* cfg;
static Inet* net;
static Iplayerdata* pd;
static Iattrman *attrman;
static Ilogman *lm;

/*
	Example:

	; Bomb level 4
	Warbird:InitialBombs = 4

	; Gun level 4
	Warbird:InitialGuns = 4

	; Thor level 2 (level 1 is a normal thor)
	Warbird:ThorLevel = 2

	; Shrap Level 1 (set to 0 to use the players current gun level aka the default)
	Warbird:ShrapLevel = 1

	; Bouncing shrapnel (set to 0 to use the players current bouncing bullet prize aka the default; set to -1 to force no bouncing shrap)
	Warbird:ShrapBounce = 1
*/

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

#define DOMINMAX(VAR, MIN, MAX) if (VAR < MIN) VAR = MIN; else if (VAR > MAX) VAR = MAX

#define SHIPS (8)
#define SETTINGS (8)
enum
{
	SETT_InitialBombs = 0,
	SETT_InitialGuns,
	SETT_ShrapLevel,
	SETT_ShrapBounce,
	SETT_ThorLevel,
	SETT_ThorShrap,
	SETT_ThorShrapLevel,
	SETT_ThorShrapBounce
};

/* cfghelp: All:InitialBombs, arena, range: 0-4
 * Initial level a ship's bombs fire.
 * Settings this to 4 will rewrite weapon packets */

/* cfghelp: All:InitialGuns, arena, int, range: 0-4
 * Initial level a ship's guns fire
 * Settings this to 4 will rewrite weapon packets */

/* cfghelp: All:ShrapLevel, arena, int, def: 0, range: 0-4
 * The shrapnel level of this ship
 * When set to non 0 weapon packets will be rewritten */

/* cfghelp: All:ShrapBounce, arena, int, def: 0
 * Wether this ship has bouncing shrapnel -1 = never; 1 = always
 * When set to non 0 weapon packets will be rewritten */


/* cfghelp: All:ThorLevel, arena, range: 0-4
 * The thor level this ship has.
 * When set to non 0 weapon packets will be rewritten */

/* cfghelp: All:ThorShrap, arena, range: 0-31
 * The amount of shrap thors have
 * When set to non 0 weapon packets will be rewritten */

/* cfghelp: All:ThorShrapLevel, arena, range: 0-4
 * The thor shrapnel level this ship has.
 * When set to non 0 weapon packets will be rewritten */

/* cfghelp: All:ThorShrapBounce, arena, int, range: 0-1, def: 0
 * Wether this ship has bouncing shrapnel for thors -1 = never;
 * 1 = always. When set to non 0 weapon packets will be rewritten */


static const char* settings[] =
{
	"InitialBombs", 
	"InitialGuns", 
	"ShrapLevel", 
	"ShrapBounce", 
	"ThorLevel", 
	"ThorShrap", 
	"ThorShrapLevel", 
	"ThorShrapBounce"
};

static const char* attrmanNames[SETTINGS] =
{
	"cset::InitialBombs",
	"cset::InitialGuns",
	"cset::ShrapLevel",
	"cset::ShrapBounce",
	"cset::ThorLevel",
	"cset::ThorShrap",
	"cset::ThorShrapLevel",
	"cset::ThorShrapBounce",
};

static pthread_mutex_t playerdatamtx = PTHREAD_MUTEX_INITIALIZER;

typedef struct RewritePlayerData
{
	signed char settings[SHIPS][SETTINGS];

} RewritePlayerData;

static int pkey = -1;
static AttrmanSetter configSetter;

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
	if (lm) lm->Log(L_ERROR | L_SYNC, "<attrman_rewrite> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
	fullsleep(500);
	Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static inline void LockPD()
{
	pthread_mutex_lock(&playerdatamtx);
}
static inline void UnlockPD()
{
	pthread_mutex_unlock(&playerdatamtx);
}


static void SetValueCB(const Target *scope, const char *attributeName, signed char changedShip, AttrmanSetter setter, ParamAttrman param)
{
	int ship;
	int setting;
	int newValue;
	int hasAbsoluteValue;
	LinkedList players = LL_INITIALIZER;
	Link *l;
	Player *p;
	Target tgtPlayer;
	RewritePlayerData *pdata;
	tgtPlayer.type = T_PLAYER;


	ship = (int) param.number;
	setting = (int) param.number2;
	
	if (changedShip != ATTRMAN_NO_SHIP && ship != changedShip) // does this ship affect this callback?
	{
		return;
	}

	assertlm(ship >= 0 && ship < SHIPS);
	assertlm(setting >= 0 && setting < SETTINGS);

	LockPD();
	pd->Lock();
	attrman->Lock();
	
	pd->TargetToSet(scope, &players);
	for (l = LLGetHead(&players); l; l = l->next)
	{
		p = l->data;
		if (!p || !IS_STANDARD(p)) continue;

		tgtPlayer.u.p = p;
		pdata = PPDATA(p, pkey);

		newValue = attrman->GetTotalValue(&tgtPlayer, attributeName, ship, &hasAbsoluteValue);
		
		if (!hasAbsoluteValue && p->arena)
		{
			newValue += cfg->GetInt(p->arena->cfg, cfg->SHIP_NAMES[ship], settings[setting], 0);
		}
		
		DOMINMAX(newValue, SCHAR_MIN, SCHAR_MAX);

		pdata->settings[ship][setting] = (signed char) newValue;
	}
	attrman->UnLock();
	pd->Unlock();
	UnlockPD();
	
	LLEmpty(&players);
}

static void updatePlayerSettings(Player *p)
{
	int ship, setting;
	RewritePlayerData *pdata;
	Target playerScope;

	assertlm(p);
	if (!p->arena) return;

	playerScope.type = T_PLAYER;
	playerScope.u.p = p;
	
	LockPD();
	attrman->Lock();
	pdata = PPDATA(p, pkey);

	for (ship = 0; ship < SHIPS; ship++)
	{
		for (setting = 0; setting < SETTINGS; setting++)
		{
			pdata->settings[ship][setting] = attrman->GetTotalValue(&playerScope, attrmanNames[setting], ship, NULL);
		}
	}
	attrman->UnLock();
	UnlockPD();
}

static void ArenaAction(Arena *arena, int action)
{
	int ship, setting;
	Target tgtArena;

	if (!arena) return;
	tgtArena.type = T_ARENA;
	tgtArena.u.arena = arena;

	if (action == AA_CREATE || action == AA_CONFCHANGED)
	{
		attrman->Lock();
		for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				SetValueCB(
					&tgtArena,
					attrmanNames[setting],
					ship,
					configSetter,
					(ParamAttrman){{(long) ship}, {(long) setting}});
			}
		}
		attrman->UnLock();
	}
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	if (newfreq != oldfreq)
		updatePlayerSettings(p);
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	if (action == PA_ENTERGAME)
	{
		updatePlayerSettings(p);
	}
}

static void AEEditPPK(Player *p, struct C2SPosition *pos)
{
	// This runs in another thread and we should lock,
	// however this is not needed because we only read here and
	// 2 values are not dependent upon each other
	LockPD();
	RewritePlayerData *pdata = PPDATA(p, pkey);

	int ship = p->p_ship;

	if (pos->weapon.type == W_BOMB || pos->weapon.type == W_PROXBOMB)
	{
		int initialbombs = pdata->settings[ship][SETT_InitialBombs];
		int shrapbounce = pdata->settings[ship][SETT_ShrapBounce];
		int shraplevel = pdata->settings[ship][SETT_ShrapLevel];
		DOMINMAX(shraplevel, 0, 4);

		if (initialbombs > 3)
			pos->weapon.level = 3;

		if (shrapbounce < 0)
			pos->weapon.shrapbouncing = 0;
		else if (shrapbounce > 0)
			pos->weapon.shrapbouncing = 1;

		if (shraplevel > 0)
			pos->weapon.shraplevel =  shraplevel - 1;
	}
	else if (pos->weapon.type == W_BULLET || pos->weapon.type == W_BOUNCEBULLET)
	{
		int initialguns = pdata->settings[ship][SETT_InitialGuns];
		if (initialguns > 3)
			pos->weapon.level = 3;
	}
	else if (pos->weapon.type == W_THOR)
	{
		int thorlevel = pdata->settings[ship][SETT_ThorLevel];
		int thorshrap = pdata->settings[ship][SETT_ThorShrap];
		int thorshraplevel = pdata->settings[ship][SETT_ThorShrapLevel];
		int thorshrapbounce = pdata->settings[ship][SETT_ThorShrapBounce];
		if (thorlevel > 0)
			pos->weapon.level = thorlevel <= 4 ? thorlevel - 1 : 3;

		if (thorshrap > 0)
		{
			DOMINMAX(thorshrap, 0, 31);
			DOMINMAX(thorshraplevel, 1, 4);
			DOMINMAX(thorshrapbounce, 0, 1);
			pos->weapon.shrap = thorshrap;
			pos->weapon.shraplevel =  thorshraplevel - 1;
			pos->weapon.shrapbouncing = thorshrapbounce;
		}

	}
	UnlockPD();
}

static Appk ppk =
{
	ADVISER_HEAD_INIT(A_PPK)
	AEEditPPK,
	NULL
};



EXPORT const char info_attrman_rewrite[] =
	"Attribute Manager Rewrite (" ASSSVERSION ", " BUILDDATE ") By JoWie <jowie@welcome-to-the-machine.com>";

static void ReleaseInterfaces()
{
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(net);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(attrman);
	mm->ReleaseInterface(lm);
}

EXPORT int MM_attrman_rewrite(int action, Imodman *mm_, Arena *arena)
{
	int ship, setting;

	if (action == MM_LOAD)
	{
		mm = mm_;

		game = mm->GetInterface(I_GAME, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		net = mm->GetInterface(I_NET, ALLARENAS);
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		attrman = mm->GetInterface(I_ATTRMAN, ALLARENAS);
		lm = mm->GetInterface(I_LOGMAN, ALLARENAS);

		if (!game || !cfg || !net || !pd || !attrman || !lm)
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}
		pkey = pd->AllocatePlayerData(sizeof(RewritePlayerData));

		if (pkey == -1)
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}
		mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);

		attrman->Lock();
		configSetter = attrman->RegisterSetter();

		for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				// Add a callback per ship; (in the old version of attrman there was no support for ships)
				// There is nothing to gain by refactoring this now
				attrman->RegisterCallback(attrmanNames[setting], SetValueCB, (ParamAttrman){{(long) ship}, {(long) setting}});
			}
		}
		attrman->UnLock();

		mm->RegAdviser(&ppk, ALLARENAS);

		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		mm->UnregAdviser(&ppk, ALLARENAS);
		attrman->Lock();
		attrman->UnregisterSetter(configSetter);

		for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				attrman->UnregisterCallback(attrmanNames[setting], SetValueCB);
			}
		}
		attrman->UnLock();

		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);

		pd->FreePlayerData(pkey);

		ReleaseInterfaces();

		return MM_OK;
	}

	return MM_FAIL;
}
