/*
ASSS Attribute Manager *super
jowie@welcome-to-the-machine.com
Copyright (c) 2009-2011  Joris v/d Wel

This file is part of ASSS Attribute Manager

   ASSS Attribute Manager is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Attribute Manager is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with ASSS Attribute Manager.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms based on section 7 of the
   GNU Affero General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version


*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


#include "asss.h"
#include "attrman.h"
#include "attrman_cset.h"

static Imodman		*mm;
static Iattrman		*attrman;
static Icmdman		*cmd;
static Ichat		*chat;
static Igame		*game;


static AttrmanSetter superSetter;

static void Csuper(const char *tc, const char *params, Player *p, const Target *target)
{
	#define SUPERVALUES \
		S("Bomb:BombDamageLevel", 0) \
		S("Bomb:EBombShutdownTime", 0) \
		S("Bomb:JitterTime", 0) \
		S("Bullet:BulletDamageLevel", 0) \
		S("Bullet:BulletDamageUpgrade", 0) \
		S("Burst:BurstDamageLevel", 0) \
		S("Burst:BurstDamageLevel", 0) \
		S("Flag:FlaggerBombFireDelay", 0) \
		S("Flag:FlaggerBombUpgrade", 1) \
		S("Flag:FlaggerBombUpgrade", 1) \
		S("Flag:FlaggerGunUpgrade", 1) \
		S("Flag:FlaggerSpeedAdjustment", 0) \
		S("Flag:FlaggerSpeedAdjustment", 0) \
		S("Flag:FlaggerSpeedAdjustment", 0) \
		S("Flag:FlaggerSpeedAdjustment", 0) \
		S("Flag:FlaggerThrustAdjustment", 0) \
		S("Misc:ExtraPositionData", 1) \
		S("Misc:SafetyLimit", 0) \
		S("Radar:MapZoomFactor", 10) \
		S("Shrapnel:InactiveShrapDamage", 0) \
		S("Soccer:AllowBombs", 1) \
		S("Soccer:AllowGuns", 1) \
		S("Team:TeamMaxMines", 255)

	#define SUPERSHIPVALUES() \
		S("AfterBurnerEnergy", 0) \
		S("AntiwarpEnergy", 0) \
		S("AntiwarpStatus", 2) \
		S("AttachBounty", 0) \
		S("BombFireDelay", 25) \
		S("BombFireEnergy", 0) \
		S("BombFireEnergyUpgrade", 0) \
		S("BrickMax", 255) \
		S("BulletFireDelay", 10) \
		S("BulletFireEnergy", 0) \
		S("BurstMax", 255) \
		S("CloakEnergy", 0) \
		S("CloakStatus", 2) \
		S("DamageFactor", 0) \
		S("DecoyMax", 255) \
		S("DisableFastShooting", 0) \
		S("InitialBombs", 4) \
		S("InitialBrick", 255) \
		S("InitialBurst", 255) \
		S("InitialDecoy", 255) \
		S("InitialEnergy", 9999) \
		S("InitialGuns", 4) \
		S("InitialPortal", 255) \
		S("InitialRecharge", 32000) \
		S("InitialRepel", 255) \
		S("InitialRocket", 255) \
		S("InitialRotation", 500) \
		S("InitialSpeed", 5000) \
		S("InitialThor", 255) \
		S("InitialThrust", 50) \
		S("LandMineFireDelay", 25) \
		S("LandmineFireEnergy", 0) \
		S("LandmineFireEnergyUpgrade", 0) \
		S("MaxBombs", 3) \
		S("MaxGuns", 3) \
		S("MaximumEnergy", 9999) \
		S("MaximumRecharge", 32000) \
		S("MaximumRotation", 500) \
		S("MaximumSpeed", 5000) \
		S("MaximumThrust", 50) \
		S("MaxMines", 255) \
		S("MultiFireDelay", 10) \
		S("MultiFireEnergy", 0) \
		S("PortalMax", 255) \
		S("RepelMax", 255) \
		S("RocketMax", 255) \
		S("SeeBombLevel", 1) \
		S("SeeMines", 1) \
		S("ShrapnelMax", 31) \
		S("ShrapnelRate", 31) \
		S("SoccerBallProximity", 64) \
		S("StealthEnergy", 0) \
		S("StealthStatus", 2) \
		S("ThorMax", 255) \
		S("ThorLevel", 4) \
		S("TurretLimit", 255) \
		S("TurretSpeedPenalty", 0) \
		S("TurretThrustPenalty", 0) \
		S("XRadarEnergy", 0) \
		S("XRadarStatus", 2) \
		S("InitialMultifire", 1) \
		S("InitialProx", 1) \
		S("InitialShrapnel", 0) \
		S("InitialBounce", 1)


	attrman->Lock();
	if (!attrman->GetValue(target, superSetter, "super::HasSuper", false, ATTRMAN_NO_SHIP))
	{ // give super
		attrman->SetValue(target, superSetter, "super::HasSuper", false, ATTRMAN_NO_SHIP, 1);

		#define S(ATTR, VALUE) attrman->SetValue(target, superSetter, "cset::" ATTR, false, ATTRMAN_NO_SHIP, VALUE);
		SUPERVALUES
		SUPERSHIPVALUES()
		#undef S
	}
	else
	{ // take away super
		attrman->UnsetValue(target, superSetter, "super::HasSuper", ATTRMAN_NO_SHIP);

		#define S(ATTR, UNUSED) attrman->UnsetValue(target, superSetter, "cset::" ATTR, ATTRMAN_NO_SHIP);
		SUPERVALUES
		SUPERSHIPVALUES()
		#undef S
	}
	attrman->UnLock();
}

static void SendClientSettingsCB(Player *p)
{
	Target tgt;
	tgt.type = T_PLAYER;
	tgt.u.p = p;

	attrman->Lock();
	if (attrman->GetValue(&tgt, superSetter, "super::HasSuper", true, ATTRMAN_NO_SHIP))
	{
		game->ShipReset(&tgt);
	}
	attrman->UnLock();
}


EXPORT const char info_super[] = "Super (" ASSSVERSION ", " BUILDDATE ") by JoWie <jowie@welcome-to-the-machine.com>\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(attrman);
        mm->ReleaseInterface(cmd    );
        mm->ReleaseInterface(chat   );
        mm->ReleaseInterface(game   );
}

EXPORT int MM_super(int action, Imodman *mm_, Arena *arena)
{
        if (action == MM_LOAD)
        {
		mm      = mm_;
		attrman = mm->GetInterface(I_ATTRMAN         , ALLARENAS);
		cmd     = mm->GetInterface(I_CMDMAN          , ALLARENAS);
		chat    = mm->GetInterface(I_CHAT            , ALLARENAS);
		game    = mm->GetInterface(I_GAME            , ALLARENAS);



 		if (!attrman || !cmd || !chat || !game)
		{
			ReleaseInterfaces();
			printf("<super> Missing interfaces");
			return MM_FAIL;
		}

		attrman->Lock();
                superSetter = attrman->RegisterSetter();
                attrman->UnLock();
                mm->RegCallback(CB_SENDCLIENTSETTINGS, SendClientSettingsCB, ALLARENAS);

                cmd->AddCommand("super", Csuper, ALLARENAS, NULL);

                return MM_OK;

        }
        else if (action == MM_UNLOAD)
        {
        	cmd->RemoveCommand("super", Csuper, ALLARENAS);
        	mm->UnregCallback(CB_SENDCLIENTSETTINGS, SendClientSettingsCB, ALLARENAS);
        	attrman->Lock();
		attrman->UnregisterSetter(superSetter);
		attrman->UnLock();
                ReleaseInterfaces();

                return MM_OK;
        }

        return MM_FAIL;
}

